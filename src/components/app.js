import React from 'react';
import SearchBar from '../containers/searchBar';
import WeatherList from "../containers/weatherList";
const App = () => {
    return (
        <div>
            <SearchBar>

            </SearchBar>
            <WeatherList>

            </WeatherList>
        </div>
    )
};
export default App;