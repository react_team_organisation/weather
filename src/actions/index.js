import axios from 'axios';

const API_KEY = 'f3d66da650eff6cf3ff94c76b3e2fc56';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city){
    const url = `${ROOT_URL}&q=${city},fr`;
    const request = axios.get(url);
    console.log('REQUEST', request);
    return {
        type: FETCH_WEATHER,
        payload: request
    };
}